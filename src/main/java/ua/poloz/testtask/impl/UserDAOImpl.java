/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.testtask.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.NotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.entity.Task;
import ua.poloz.entity.User;
import ua.poloz.testtask.dao.UserDAO;

/**
 *
 * @author User
 */
@Repository
public class UserDAOImpl implements UserDAO {

    private static Logger log = Logger.getLogger(UserDAOImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public User getUser(User user) {
        log.info("Get user: login: " + user);
        return (User) sessionFactory.getCurrentSession().load(User.class,
                user.getLogin());
    }

    @Override
    public void createUser(String login, String password) {
        log.info("Create user: login: " + login + ", password:  " + password);
        User user = new User(login, password);
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void deleteUser(String login) {
        log.info("Delete user: login: " + login);
        Task task = (Task) sessionFactory.getCurrentSession().load(Task.class,
                login);
        if (null != task) {
            sessionFactory.getCurrentSession().delete(task);
        } else {
            try {
                throw new NotFoundException("Not supported yet.");
            } catch (NotFoundException ex) {
                Logger.getLogger(TaskDAOImpl.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<User> getUsers() {
        log.info("Get users");
        return sessionFactory.getCurrentSession().createQuery("from User").
                list();
    }

}
