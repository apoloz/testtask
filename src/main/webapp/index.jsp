<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <div style="margin:10px;">
            <h1>Task Management</h1>
            <p><a href="login">Login</a></p>
            <p><a href="createUser">Create new user</a></p>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
