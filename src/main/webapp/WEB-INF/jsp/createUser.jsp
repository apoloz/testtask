	
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <div style="margin:10px;">
            <h3>Create new user. Enter your login and password</h3>
            <form action="createUser" method="post">
                <spring:bind path="value">
                    <br/>Username:<input type="text" name="login">
                </spring:bind>
                <br/>Password:<input type="password" name="password">
                <br/><input type="submit" value="Create new user">
            </form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
