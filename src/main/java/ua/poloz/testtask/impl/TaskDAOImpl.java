/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.testtask.impl;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.NotFoundException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.entity.Task;
import ua.poloz.testtask.dao.TaskDAO;

/**
 *
 * @author User
 */
@Repository
public class TaskDAOImpl implements TaskDAO {

    private static Logger log = Logger.getLogger(TaskDAOImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Task getTask(Integer id) {
        return (Task) sessionFactory.getCurrentSession().load(Task.class,
                id);
    }

    @Override
    public void createTask(Task task) {
        log.info("Create task: " + task.toString());
        if (task.getStartDate() == null) {
            task.setStartDate(new Date(Calendar.getInstance().
                    getTimeInMillis()));
        }
        sessionFactory.getCurrentSession().save(task);
    }

    @Override
    public void updateTask(Task task) {
        log.info("Update task: " + task.toString());
        if (task.getStartDate() == null) {
            task.setStartDate(new Date(Calendar.getInstance().
                    getTimeInMillis()));
        }
        Task uTask = (Task) sessionFactory.getCurrentSession().
                load(Task.class, task.getId());

        sessionFactory.getCurrentSession().merge(uTask);
    }

    private static final int limitResultsPerPage = 20;

    @Override
    public List<Task> getTasks(int page) {
        log.info("Get tasks");
        Query q = sessionFactory.getCurrentSession().createQuery(
                "from Task");
        q.setFirstResult(page * limitResultsPerPage);
        q.setMaxResults(limitResultsPerPage);
        return (List<Task>) q.list();
    }

    @Override
    public void deleteTask(Integer id) {
        log.info("Delete task: id = " + id);
        Task task = (Task) sessionFactory.getCurrentSession().load(Task.class,
                id);
        log.info("Delete task: " + task.toString());
        if (null != task) {
            sessionFactory.getCurrentSession().delete(task);
        } else {
            try {
                throw new NotFoundException("Not supported yet.");
            } catch (NotFoundException ex) {
                Logger.getLogger(TaskDAOImpl.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Task> filterTasks(Date startDate, Date endDate) {
        log.info("Get filter. Start date: " + startDate + ", end date: " + endDate);
        String hql = "FROM Task E WHERE E.startDate > " + startDate + " AND E.startDate < " + endDate +" ORDER BY E.startDate DESC";
        Query q = sessionFactory.getCurrentSession().createQuery(
                hql);
        return (List<Task>) q.list();
    }

}
