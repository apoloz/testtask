<%@page contentType="text/html" pageEncoding="UTF-8"%>		
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <body>
            <h3>List task</h3>
            <c:if test="${!empty listTasks}">
                <table class="data" border="1">
                    <tr>
                        <td>id</td>
                        <td>Theme</td>
                        <td>Description</td>
                        <td>State</td>
                        <td>Date</td>
                        <td>Edit</td>
                        <td>View</td>
                    </tr>
                    <c:forEach var="tasks" items="${requestScope.listTasks}" varStatus="task">
                        <tr>
                            <td>${tasks.id}</td>
                            <td>${tasks.theme}</td>
                            <td>${tasks.description}</td>
                            <td>${tasks.stateTask}</td>
                            <td>${tasks.startDate}</td>
                            <td><a href="readTask/${tasks.id}">edit</a></td>
                            <td><a href="viewTask/${tasks.id}">view</a></td>
                        </tr>
                    </c:forEach>
                </table>

            </c:if>
            <div class="pagination">
                    <ul>
                        <li class="disabled"><a href="0">First</a></li>
                        <li class="disabled"><a href="#">Prev</a></li>
                        <li class="active"><a href="0">1</li>
                        <li class="active"><a href="1">2</li>
                        <li class="active"><a href="2">3</a></li>
                        <li class="active"><a href="3">4</a></li>
                        <li class="active"><a href="4">5</a></li>
                        <li class="active"><a href="#">Next</a></li>
                        <li class="active"><a href="#">Last</a></li>
                    </ul>
                </div>
            <p><a href="createTask">New task</a></p>
        </body>
    </tiles:putAttribute>
</tiles:insertDefinition>
