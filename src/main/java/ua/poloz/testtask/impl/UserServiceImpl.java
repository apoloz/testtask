/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.testtask.impl;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.poloz.entity.User;
import ua.poloz.service.UserService;
import ua.poloz.testtask.dao.UserDAO;

/**
 *
 * @author poloz
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Transactional
    @Override
    public User getUser(User user) {
        return userDAO.getUser(user);
    }

    @Transactional
    @Override
    public void createUser(String login, String password) {
        userDAO.createUser(login, password);
    }

    @Transactional
    @Override
    public void deleteUser(String login) {
        userDAO.deleteUser(login);
    }

    @Transactional
    @Override
    public void createUser(User user) {
        userDAO.createUser(user.getLogin(), user.getPassword());
    }

    @Transactional
    @Override
    public List<User> getUsers() {
        return userDAO.getUsers();
    }

}
