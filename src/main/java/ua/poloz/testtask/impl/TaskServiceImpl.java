/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.testtask.impl;

import java.sql.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.poloz.entity.Task;
import ua.poloz.service.TaskService;
import ua.poloz.testtask.dao.TaskDAO;

/**
 *
 * @author poloz
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Transactional
    @Override
    public Task getTask(Integer id) {
        return taskDAO.getTask(id);
    }

    @Transactional
    @Override
    public void createTask(Task task) {
        taskDAO.createTask(task);
    }

    @Transactional
    @Override
    public void updateTask(Task task) {
        taskDAO.updateTask(task);
    }

    @Transactional
    @Override
    public List<Task> getTasks(int page) {
        return taskDAO.getTasks(page);
    }

    @Transactional
    @Override
    public void deleteTask(Integer id) {
        taskDAO.deleteTask(id);
    }

    @Override
    public List<Task> filterTasks(Date startDate, Date endDate) {
        return taskDAO.filterTasks(startDate, endDate);
    }
    
}
