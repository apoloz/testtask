/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.poloz.entity.Task;
import ua.poloz.service.TaskService;

/**
 *
 * @author poloz
 */
@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/createTask")
    public String createTask() {
        return "createTask";
    }

    @RequestMapping(value = "/createTask", method = RequestMethod.POST)
    public String createTask(@ModelAttribute("task") Task task,
                             BindingResult result) {
        taskService.createTask(task);
        return "redirect:/allTasks/0";
    }

    @RequestMapping(value = "/allTasks/viewTask/{id}")
    public String viewTask(@PathVariable("id") int id, Model model) {
        model.addAttribute("task", taskService.getTask(id));
        return "viewTask";
    }

    @RequestMapping(value = "/allTasks/readTask/{id}", method = RequestMethod.GET)
    public String readTask(@PathVariable("id") int id, Model model) {
        model.addAttribute("task", taskService.getTask(id));
        return "readTask";
    }
    
    @RequestMapping(value = "/allTasks/readTask/createTask", method = RequestMethod.POST)
    public String editTask(@ModelAttribute("task") Task task,
                             BindingResult result) {
        System.out.println(task.getId() + task.toString());
        taskService.updateTask(task);
        return "redirect:/allTasks/0";
    }

    @RequestMapping(value = "/readTask")
    public String readTask() {
        return "readTask";
    }

    @RequestMapping(value = "/allTasks/{page}")
    public ModelAndView viewPersons(@PathVariable("page") int page) {
        Map<String, List<Task>> listTasks = new HashMap<>();
        listTasks.put("listTasks", taskService.getTasks(page));
        return new ModelAndView("/allTasks", listTasks);
    }

}
