/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.service;

import java.util.List;
import ua.poloz.entity.User;

/**
 *
 * @author poloz
 */
public interface UserService {

    public User getUser(User user);

    public void createUser(String login, String password);

    public void deleteUser(String login);

    public void createUser(User user);

    public List<User> getUsers();

}
