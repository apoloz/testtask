<%@page contentType="text/html" pageEncoding="UTF-8"%>	
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <div style="margin:10px;">
            <h3>Edit a task. Complete all fields</h3>
            <form action="createTask" method="post">

                <h3>View task</h3>
                <table>
                    <tr>
                        <td><i>ID: </i></td>
                        <td><input name="id" type="text" value="${task.id}" ></td>
                    </tr>
                    <tr>
                        <td><i>Theme: </i></td>
                        <td><input name="theme" type="text" value="${task.theme}"></td>
                    </tr>
                    <tr>
                        <td><i>Description: </i></td>
                        <td><input name="description" type="text" value="${task.description}"></td>
                    </tr>
                    <tr>
                        <td><i>State: </i></td>
                        <td><select name="stateTask">
                                <option>appointed</option>
                                <option>in the process</option>
                                <option>testing</option>
                                <option>closed</option>
                            </select></td>
                    </tr>

                </table>
                <br/><input type="submit" value="Save task">
            </form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>

