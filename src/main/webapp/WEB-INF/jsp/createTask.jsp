<%@page contentType="text/html" pageEncoding="UTF-8"%>	
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <div style="margin:10px;">
            <h3>Create new task. Complete all fields</h3>
            <form action="createTask" method="post">
                <spring:bind path="value">
                    <br/>Theme:<input type="text" name="theme">
                </spring:bind>
                <spring:bind path="value">
                    <br/>Description:
                    <br/><textarea name="description" cols="40" rows="3"></textarea>
                </spring:bind>
                <spring:bind path="value">
                    <br/>State:<select name="stateTask">
                        <option>appointed</option>
                        <option>in the process</option>
                        <option>testing</option>
                        <option>closed</option>
                    </select>
                </spring:bind>
                
                <br/><input type="submit" value="Create new task">
            </form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
