/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.testtask.dao;

import java.sql.Date;
import java.util.List;
import ua.poloz.entity.Task;

/**
 *
 * @author User
 */
public interface TaskDAO {
    
    public Task getTask(Integer id);
    
    public void createTask(Task task);
    
    public  void updateTask(Task task);
    
    public List<Task> getTasks(int page);
    
    public List<Task> filterTasks(Date startDate, Date endDate);
    
    public void deleteTask(Integer id);
    
}
