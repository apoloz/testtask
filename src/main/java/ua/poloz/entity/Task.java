/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

/**
 *
 * @author poloz
 */
@Entity
@Table(name = "tasks")
@Proxy(lazy=false)
public class Task implements Serializable {
    
    public Task(){
    }

    public Task(String id, String theme, String description, String stateTask, String startDate){
        System.out.println("String Task");
        this.id = Integer.getInteger(id);
        this.theme = theme;
        this.description = description;
        this.stateTask = stateTask;
        this.startDate = Date.valueOf(startDate);
    }
    
    @Id
    @GeneratedValue
    private Integer id;

    private String theme;

    private String description;

    private String stateTask;

    private Date startDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * @param theme the theme to set
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the stateTask
     */
    public String getStateTask() {
        return stateTask;
    }

    /**
     * @param stateTask the state to set
     */
    public void setStateTask(String stateTask) {
        this.stateTask = stateTask;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the date to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "Task teme: " + getTheme() + ", state: " + getStateTask() + ", date: " + getStartDate();
    }

}
