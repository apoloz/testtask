/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.poloz.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.poloz.entity.User;
import ua.poloz.service.UserService;

/**
 *
 * @author poloz
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String home() {
        return "redirect:/jsp/login";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/createUser")
    public String createUser() {
        return "createUser";
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("User") User user,
                             BindingResult result) {
        userService.createUser(user);
        return "redirect:/login";
    }

    @RequestMapping(value = "/loginUser", method = RequestMethod.POST)
    public String loginUser(@ModelAttribute("User") User user,
                            BindingResult result) {
        boolean test = false;
        List<User> users = userService.getUsers();
        for (User fUser : users) {
            if (user.equals(fUser)) {
                test = true;
            }
        }
        if (test == true) {
            return "redirect:/allTasks/0";
        } else {
            return "badLogin";
        }
    }

    @RequestMapping("/deleteUser/{login}")
    public String deleteUser(@PathVariable("login") String login) {

        userService.deleteUser(login);

        return "redirect:/login";
    }

}
