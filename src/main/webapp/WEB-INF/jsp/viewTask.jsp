<%@page contentType="text/html" pageEncoding="UTF-8"%>		
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="DefaultTemplate">
    <tiles:putAttribute name="body">
        <div style="margin:10px;">
            <h3>View task</h3>
            <table>
                <tr>
                    <td><i>ID: </i></td>
                    <td>${task.id}</td>
                </tr>
                <tr>
                    <td><i>Theme: </i></td>
                    <td>${task.theme}</td>
                </tr>
                <tr>
                    <td><i>Description: </i></td>
                    <td>${task.description}</td>
                </tr>
                <tr>
                    <td><i>State: </i></td>
                    <td>${task.stateTask}</td>
                </tr>
                <tr>
                    <td><i>Date: </i></td>
                    <td>${task.startDate}</td>
                </tr>
            </table>
        </div>
        <p><a href="/TestTask/allTasks/0">All Task</a></p>
    </tiles:putAttribute>
</tiles:insertDefinition>

